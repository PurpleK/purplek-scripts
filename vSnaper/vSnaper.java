package vSnaper;

import org.osbot.script.MessageListener;
import org.osbot.script.MouseListener;
import org.osbot.script.Script;
import org.osbot.script.ScriptManifest;
import org.osbot.script.rs2.map.Position;
import org.osbot.script.rs2.model.GroundItem;
import org.osbot.script.rs2.model.NPC;
import org.osbot.script.rs2.ui.RS2Interface;
import org.osbot.script.rs2.utility.Area;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Purple
 * Date: 7/17/13
 * Time: 4:25 AM
 */
@ScriptManifest(author = "PurpleK", name = "vSnaper", version = 1.08,
        info = "Info: vSnaper is snape grass collector that uses Waterbirth and Rellekka NPC banking support for decent money per hour . \n\nIncludes: a coins and snape grass anti-lure!.\n\n" +
                "Requirements: Must be able to bank in Rellekka via Peer the Seer and travel to Waterbirth via the 'Travel' option on Jarvald.\n\nFeatures:\n- Error debugging, will inform you why the script has stopped.\nex; Manul script stop, ran out of coins\n-Multi Threaded to perferm extra tasks while botting such as calculations and antiban\n" +
                "-Guaranteed to be faster than any other snape grass farming bot!")
public class vSnaper extends Script implements MouseListener, MessageListener {

    public ScriptManifest PROPS = getClass().getAnnotation(ScriptManifest.class);



    public SnapeGUI snapeGUI = new SnapeGUI();
    public ErrorGUI errorGUI = new ErrorGUI();
    public VersionGUI versionGUI = new VersionGUI();

    private final MultiTask multitasker = new MultiTask();
    private final Thread t = new Thread(multitasker);

    private final Color MOUSE_COLOR = new Color(0, 153, 204, 120),
            MOUSE_BORDER_COLOR = new Color(0, 153, 204);

    private final Color TEXT_COLOR = new Color(20, 185, 89);
    private final Color BG_COLOR = new Color(51, 51, 51, 225);
    private final Font FONT_SMALL = new Font("Arial", 0, 12);

    private boolean showGUI,
            hidePaint,
            errorHappened,
            basicPaint = true,
            stopped,
            picked,
            checkedVersion,
            usePolygons,
            useRun;

    //Strings
    private String status;

    //Longs
    private long timeRunning,
            startTime,
            lastingTime,
            snapeCollected,
            versionTime,
            startVersionTime,
            dropValue;

    //Ints
    private int
            nextFreeSlot,
            currFreeSlot;

    private  int upX = 538,
            upY = 294,
            dragUpX,
            dragUpY;

    private Rectangle paintBox;
    private Rectangle minimizeBox = new Rectangle(474, 350, 34, 18);

    public final MouseTrail mouseTrail;
    public vSnaper() {
        mouseTrail = new MouseTrail();
    }

    private Image getImage(String url) {
        try {
            return ImageIO.read(new URL(url));
        } catch(IOException e) {
            return null;
        }
    }

    private final Area snapeOne = new Area(2553, 3751, 2553, 3751);
    private final Area snapeTwo = new Area(2553, 3754, 2553, 3754);
    private final Area snapeThree = new Area(2552, 3757, 2552, 3757);
    private final Area snapeFour = new Area(2546, 3763, 2546, 3763);
    private final Area snapeFive = new Area(2542, 3765, 2542, 3765);
    private final Area snapeSix = new Area(2540, 3765, 2540, 3765);

    private Area SGsnape = new Area(2535, 3735, 2556, 3769);
    private final Area SGfremmy = new Area(2610, 3654, 2654, 3699);
    private  Area coinsPosition;
    private Area SGcoins;

    private final Image paint = getImage("http://i.imgur.com/XqhBObg.png");
    private final Image maximizePaint = getImage("http://i.imgur.com/UyFpx2q.png");

    public static enum State{
        //GUI Enums
        NULL, GUI, STARTED, NOTHING, ENABLE_RUN,
        PICK_UP, TRAVEL_ISLE, TRAVEL_FREMMY, BANK_ITEMS,
        ERROR
    }

    private State getStateGUI() throws InterruptedException {
        if(!snapeGUI.isShowing() && showGUI && !versionGUI.isShowing()) {
            return State.GUI;
        } else if(!snapeGUI.isShowing() && !showGUI) {
                return State.STARTED;
        }
        return State.NULL;
    }

    @Override
    public void onStart() {
        dropValue = client.getInventory().getItemForName("Coins").getAmount();
        snapeAreas.add(snapeOne);
        snapeAreas.add(snapeTwo);
        snapeAreas.add(snapeThree);
        snapeAreas.add(snapeFour);
        snapeAreas.add(snapeFive);
        snapeAreas.add(snapeSix);
        nextFreeSlot = client.getInventory().getFirstEmptySlot();
        currFreeSlot = client.getInventory().getFirstEmptySlot();
        picked = false;
        SGcoins = new Area(client.getMyPlayer().getX() - 3, client.getMyPlayer().getY() - 3, client.getMyPlayer().getX() + 3, client.getMyPlayer().getY() + 3);
        status = "Initializing";
        showGUI = true;
        startTime = System.currentTimeMillis();
        log("Welcome to vSnaper " + PROPS.version() + "!");
        warn("ATTENTION!");
        warn("Please don't abuse this script with multiple accounts and crash the market!");
    }

    @Override
    public void onExit() {
        versionGUI.dispose();
        errorGUI.dispose();
        snapeGUI.dispose();
        log("Thanks for vSnaper " + PROPS.version() + " ! ~ <3 Purple");
        if(!errorHappened) {
            errorGUI.reasonLabel.setText(errorGUI.reasonLabel.getText() + "Manual Script Stop!");
            errorGUI.collectedLabel.setText(errorGUI.collectedLabel.getText() + NumberFormat.getIntegerInstance().format(snapeCollected));
            errorGUI.runtimeLabel.setText(errorGUI.runtimeLabel.getText() + formatTime(timeRunning));
        }
        errorGUI.setTitle("Results!");
        errorGUI.setResizable(false);
        errorGUI.setVisible(true);
        stopped = true;
    }

    @Override
    public int onLoop() throws InterruptedException {
        try {
            switch(getStateGUI()) {
                case GUI:
                    if(!versionGUI.isShowing()) {
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                snapeGUI.instructionText.setEditable(false);
                                snapeGUI.instructionText.setLineWrap(true);
                                snapeGUI.instructionText.setFocusable(false);
                                snapeGUI.instructionText.setBackground(new Color(0, 0, 0, 0));
                                snapeGUI.instructionText.setForeground(Color.cyan);
                                snapeGUI.instructionText.setFont(new Font("Verdana", Font.PLAIN, 10));
                                snapeGUI.instructionText.setWrapStyleWord(true);
                                snapeGUI.instructionText.setText("Must have started fremmy trials and be able to bank via Peer the Seer NPC and have at least already visited moon isle once. Coins required in inventory.");
                                snapeGUI.instructionScrollPane.setViewportView(snapeGUI.instructionText);
                                snapeGUI.setVisible(true);
                            }
                        });
                    }
                    sleep(500 + random(200, 600));
                    break;
                case STARTED:
                    if(!t.isAlive()) {
                        t.setDaemon(true);
                        t.start();
                    } else if(t.isAlive()) {
                    snapeGrassSwitch();
                    }
                    break;
                case NULL:
                    status = "Null State";
                    sleep(random(550, 750));
                    break;
            }
        }
        catch (InterruptedException e) {
            throw e;
        }
        catch (Exception e) {
            e.printStackTrace();
            return random(300, 400);
        }
        return random(50,150);
    }

    private State getStateSnape() throws InterruptedException {

        int animation = myPlayer().getAnimation();

        switch(animation) {
            case -1:
                if(hasNoCoins()) {
                    return State.ERROR;
                } else if(canToggleRun() && useRun) {
                    return State.ENABLE_RUN;
                } else if(inArea(SGsnape, 0) && !client.getInventory().isFull()) {
                    return State.PICK_UP;
                } else if(inArea(SGsnape, 0) && client.getInventory().isFull()) {
                    return State.TRAVEL_FREMMY;
                } else if(inArea(SGfremmy, 0) && client.getInventory().isFull() || inArea(SGfremmy, 0) && client.getInventory().getEmptySlots() <= 1) {
                    return State.BANK_ITEMS;
                } else if(inArea(SGfremmy, 0) && !client.getInventory().contains("Snape grass")) {
                    return State.TRAVEL_ISLE;
                }
            }
        return State.NOTHING;
    }

    public void snapeGrassSwitch() throws InterruptedException {

        final NPC jarvald = closestNPCForName("Jarvald");
        final NPC seer = closestNPCForName(SGfremmy, "Peer the Seer");
        final GroundItem coins = closestGroundItemForName(SGcoins, "Coins");

        switch(getStateSnape()) {
            case ERROR:
                status = "Logging out!";
                error("No coins detected!");
                break;
            case ENABLE_RUN:
                status = "Enabling running";
                setRunning(true);
                break;
            case PICK_UP:if(hasSailInterface()) {
                status = "Sailing to Waterbirth";
            } else if(getClosestSnape().exists() && !myPlayer().isMoving() && !hasSailInterface()) {
                status = "Collecting snape grass";
                if(getClosestSnape().isVisible()) {
                    getClosestSnape().interact("Take", 0, false);
                    waitFor(new Condition() {
                        @Override
                        public boolean active() {
                            return !myPlayer().isMoving() || getClosestSnape().equals(myPlayer().getPosition());
                        }
                    }, random(600, 1200));
                } else if(!getClosestSnape().isVisible()) {
                    getClosestSnape().interact("Take", 0 , true);
                    waitFor(new Condition() {
                        @Override
                        public boolean active() {
                            return !myPlayer().isMoving() || getClosestSnape().equals(myPlayer().getPosition());
                        }
                    }, random(600, 1200));
                }
            }
                break;
            case TRAVEL_FREMMY:
                if(jarvald.exists()) {
                    if(!hasOptionInterface() && !myPlayer().isFacing(jarvald)) {
                        status = "Talking to Jarvald";
                        jarvald.interact("Travel");
                        waitFor(new Condition() {
                            @Override
                            public boolean active() {
                                return hasOptionInterface();
                            }
                        }, random(1000, 2500));
                    } else if(hasOptionInterface()) {
                        if(client.getInterface(228).getChild(1) != null) {
                            status = "Advancing dialogue";
                            client.getInterface(228).getChild(1).interact("Continue");
                            waitFor(new Condition() {
                                @Override
                                public boolean active() {
                                    return inArea(SGfremmy, 0);
                                }
                            }, random(1000, 2500));
                        }
                    }
                }
                break;
            case BANK_ITEMS:
                if(hasSailInterface()) {
                    status = "Sailing to Rellekka";
                } else if(myPlayer().getY() >= 3680 && !hasSailInterface()) {
                    status = "Walking to Peer the Seer";
                    walkMiniMap(new Position(2628 + random(1,4), 3667 + random(1, 5), 0));
                    waitFor(new Condition() {
                        @Override
                        public boolean active() {
                            return seer.isVisible();
                        }
                    }, random(1000, 2500));
                }   else if(client.getInventory().contains("Coins") && seer.isVisible() && seer.getPosition().distance(myPosition()) <= 5) {
                    status = "Dropping coins stack";
                    if(client.getInventory().contains("Coins")) {
                        dropValue = client.getInventory().getItemForName("Coins").getAmount();
                    }
                    client.getInventory().interactWithName("Coins", "Drop");
                    SGcoins = new Area(client.getMyPlayer().getX() - 1, client.getMyPlayer().getY() - 1, client.getMyPlayer().getX() + 1, client.getMyPlayer().getY() + 1);
                    coinsPosition = new Area(client.getMyPlayer().getX(), client.getMyPlayer().getY(),client.getMyPlayer().getX(), client.getMyPlayer().getY());
                    coinsArea.add(coinsPosition);
                    waitFor(new Condition() {
                        @Override
                        public boolean active() {
                            return !client.getInventory().contains("Coins");
                        }
                    }, random(1000, 2500));
                }   else  if(seer.exists()) {
                    if(!hasChatInterface() && !hasNPCChatInterface() && !hasOptionInterface() && !hasPeerChatInterface() && !hasPeerNPCChatInterface() && !hasSailInterface() && !myPlayer().isFacing(seer)) {
                        if(seer.isVisible()) {
                            status = "Talking to Peer the Seer";
                        } else {
                            status = "Walking to Peer the Seer";
                        }
                        seer.interact("Talk-to");
                        waitFor(new Condition() {
                            @Override
                            public boolean active() {
                                return seer.isVisible();
                            }
                        }, random(550, 1234));
                    } else if(hasPeerChatInterface()) {
                        status = "Advancing dialogue";
                        client.getInterface(65).getChild(4).interact("Continue");
                        waitFor(new Condition() {
                            @Override
                            public boolean active() {
                                return !hasPeerChatInterface();
                            }
                        }, random(550, 1234));
                    } else if(hasPeerNPCChatInterface()) {
                        if(client.getInterface(243).getChild(5) != null) {
                            status = "Advancing dialogue";
                            client.getInterface(243).getChild(5).interact("Continue");
                            waitFor(new Condition() {
                                @Override
                                public boolean active() {
                                    return !hasPeerNPCChatInterface();
                                }
                            }, random(550, 1234));
                        }
                    } else if(hasOptionInterface()) {
                        if(client.getInterface(228).getChild(1) != null) {
                            status = "Advancing dialogue";
                            client.getInterface(228).getChild(1).interact("Continue");
                            waitFor(new Condition() {
                                @Override
                                public boolean active() {
                                    return !hasOptionInterface();
                                }
                            }, random(1000, 2500));
                        }
                    } else if(hasChatInterface()) {
                        if(client.getInterface(64).getChild(3) != null) {
                            status = "Advancing dialogue";
                            client.getInterface(64).getChild(3).interact("Continue");
                            waitFor(new Condition() {
                                @Override
                                public boolean active() {
                                    return !client.getInventory().isFull();
                                }
                            }, random(1000, 2500));
                        }
                    }
                }
                break;
            case TRAVEL_ISLE:
                if(coins != null && !client.getInventory().contains("Coins") || coins != null && client.getInventory().getItemForName("Coins").getAmount() < dropValue) {
                    status = "Picking up our cash stack";
                    coins.interact("Take");
                    waitFor(new Condition() {
                        @Override
                        public boolean active() {
                            return client.getInventory().getItemForName("Coins").getAmount() > dropValue;
                        }
                    }, random(150, 375));
                } else if(myPlayer().getY() <= 3667 || myPlayer().getX() >= 2637) {
                    status = "Walking to Jarvald";
                    walkMiniMap(new Position(2628 + random(1,2), 3671 + random(1,3), 0));
                } else if(jarvald.exists()) {
                    if(!jarvald.isVisible()) {
                        status = "Walking to Jarvald";
                        jarvald.interact("Travel");
                        waitFor(new Condition() {
                            @Override
                            public boolean active() {
                                return jarvald.isVisible();
                            }
                        }, random(1000, 2500));
                    } else if(!hasJarvaldInterface() && !hasOptionInterface() && !myPlayer().isFacing(jarvald)) {
                        status = "Talking to Jarvald";
                        jarvald.interact("Travel");
                        waitFor(new Condition() {
                            @Override
                            public boolean active() {
                                return hasJarvaldInterface();
                            }
                        }, random(1000, 2500));
                    } else if(hasJarvaldInterface()) {
                        if(client.getInterface(242).getChild(1) != null) {
                            status = "Advancing dialogue";
                            client.getInterface(242).getChild(4).interact("Continue");
                            waitFor(new Condition() {
                                @Override
                                public boolean active() {
                                    return !hasJarvaldInterface();
                                }
                            }, random(1000, 2500));
                        }
                    } else if(hasOptionInterface()) {
                        if(client.getInterface(228).getChild(1) != null) {
                            status = "Advancing dialogue";
                            client.getInterface(228).getChild(1).interact("Continue");
                            waitFor(new Condition() {
                                @Override
                                public boolean active() {
                                    return hasSailInterface();
                                }
                            }, random(1000, 2500));
                        }
                    }
                }
                break;
            case NOTHING:
                status = "Logic Broken ";
                sleep(200 + random(400, 500));
                break;
        }
    }

    @Override
    public void onPaint(Graphics g) {
        ((Graphics2D)g).setRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));

        timeRunning = System.currentTimeMillis() - startTime;
        versionTime =  System.currentTimeMillis() - startVersionTime;

        if(versionTime >= 2700000) {
            checkVersion();
        }

        if(nextFreeSlot != client.getInventory().getFirstEmptySlot() && inArea(SGsnape, 0)) {
            nextFreeSlot = client.getInventory().getFirstEmptySlot();
            picked = true;
        }

        if(picked && nextFreeSlot != currFreeSlot && inArea(SGsnape, 0)) {
            picked = false;
            snapeCollected++;
        } else {
            currFreeSlot = client.getInventory().getFirstEmptySlot() - 1;
        }


        //if(t.isAlive()) {
        //    g.drawString("Coins Value: " + String.valueOf(dropValue), 250, 250);
        //}
        //    g.drawString("UpY: " + String.valueOf(upY), 250, 267);
        //}

        if(hidePaint == false && showGUI == false && basicPaint == true) {
            g.drawImage(paint, upX,upY, null);
            g.setColor(TEXT_COLOR);
            g.setFont(FONT_SMALL);
            g.drawString("Snape Grass Collected: " + NumberFormat.getIntegerInstance().format(snapeCollected), upX + 9, upY + 44);
            g.drawString("Snape Grass Per  /hour: " + NumberFormat.getIntegerInstance().format(snapeCollected * 3600000D / (System.currentTimeMillis() - startTime)), upX + 9, upY + 61);
            g.drawString("Elapsed Time: " + formatTime(timeRunning), upX + 9, upY + 78);
            g.drawString("Status: " + status, upX + 9, upY + 96);
        }

        if(hidePaint == true && showGUI == false && basicPaint == true) {
            //g.setColor(BG_COLOR);
            //g.fillRect(upX, upY, 200, 25);
            //g.setColor(TEXT_COLOR);
            //g.drawRect(upX, upY, 200, 25);
            //g.setFont(FONT);
            //g.drawString("Click To Show Paint", upX + 33, upY + 19);
            g.drawImage(maximizePaint, upX, upY , null);
        }

        if(coinsOnGround() != null && usePolygons && !hidePaint) {
            try {
                drawCoinsArea(g);
            }
            catch (Throwable t) {
            }
        }

        if(inArea(SGsnape, 0) && usePolygons && !hidePaint) {
            try {
                drawSnapeTile(g);
            }
            catch (Throwable t) {
            }
        }

        //drawMouseEffect(g);
        mouseTrail.add(client.getMousePosition());
        mouseTrail.draw(g);
        drawMouse(g);
    }

    private ArrayList<Area> snapeAreas = new ArrayList();
    private ArrayList<Area> coinsArea = new ArrayList();


    private GroundItem getClosestSnape() {
        ArrayList<Area> snapes = new ArrayList();
        snapes.addAll(snapeAreas);

        while (snapes.size() > 0) {
            int dist = 9001;
            Area a = null;
            for (Area area : snapes) {
                if (myPosition().distance(area.getRandomPosition(0)) < dist) {
                    dist = myPosition().distance(area.getRandomPosition(0));
                    a = area;
                }
            }
            GroundItem gi = closestGroundItemForName(a, "Snape grass");
            if (gi != null) {
                return gi;
            }
            snapes.remove(a);
        }
        return null;
    }

    private GroundItem coinsOnGround() {
        ArrayList<Area> coins = new ArrayList();
        coins.addAll(coinsArea);

        while (coins.size() > 0) {
            int dist = 9001;
            Area a = null;
            for (Area area : coins) {
                if (myPosition().distance(area.getRandomPosition(0)) < dist) {
                    dist = myPosition().distance(area.getRandomPosition(0));
                    a = area;
                }
            }
            GroundItem gi = closestGroundItemForName(a, "Coins");
            if (gi != null) {
                return gi;
            }
            coins.remove(a);
        }
        return null;
    }

    public void drawSnapeTile(final Graphics g) {

        Position snapeTile = getClosestSnape().getPosition();
        Polygon snapePoly = snapeTile.getPolygon(client.getBot());

        int px = 0;
        int py = 0;

        if(getClosestSnape() != null && getClosestSnape().isVisible()) {


            for (int i = 0; i < snapePoly.npoints; i++) {
                px = snapePoly.xpoints[i]; py = snapePoly.ypoints[i];
            }

            if (!hidePaint) {
                g.setColor(BG_COLOR);
                g.drawPolygon(snapePoly);
                g.setColor(new Color(0, 153, 204, 120));
                g.setColor(Color.cyan);
                g.drawString(String.valueOf(getClosestSnape().getName()), px, py - 25);

            }
        }
    }

    public void drawCoinsArea(final Graphics g) {


        Position coinsTile = coinsOnGround().getPosition();
        Polygon coinCenter = coinsTile.getPolygon(client.getBot());

        Position topLeft = new Position(coinsTile.getX() - 1, coinsTile.getY() + 1, coinsTile.getZ());
        Polygon coinsTopLeft = topLeft.getPolygon(client.getBot());

        Position topCenter = new Position(coinsTile.getX(), coinsTile.getY() + 1, coinsTile.getZ());
        Polygon coinsTopCenter = topCenter.getPolygon(client.getBot());

        Position topRight = new Position(coinsTile.getX() + 1, coinsTile.getY() + 1, coinsTile.getZ());
        Polygon coinsTopRight = topRight.getPolygon(client.getBot());

        Position leftCenter = new Position(coinsTile.getX() - 1, coinsTile.getY(), coinsTile.getZ());
        Polygon coinsLeftCenter = leftCenter.getPolygon(client.getBot());

        Position rightCenter = new Position(coinsTile.getX() + 1, coinsTile.getY(), coinsTile.getZ());
        Polygon coinsRightCenter = rightCenter.getPolygon(client.getBot());

        Position bottomLeft = new Position(coinsTile.getX() - 1, coinsTile.getY() - 1, coinsTile.getZ());
        Polygon coinsBottomLeft = bottomLeft.getPolygon(client.getBot());

        Position bottomCenter = new Position(coinsTile.getX(), coinsTile.getY() - 1, coinsTile.getZ());
        Polygon coinsBottomCenter = bottomCenter.getPolygon(client.getBot());

        Position bottomRight = new Position(coinsTile.getX() + 1, coinsTile.getY() - 1, coinsTile.getZ());
        Polygon coinsBottomRight = bottomRight.getPolygon(client.getBot());


        int px = 0;
        int py = 0;

        for (int i = 0; i < coinCenter.npoints; i++) {
            px = coinCenter.xpoints[i]; py = coinCenter.ypoints[i];
        }

        if(coinsOnGround() != null) {

            if (!hidePaint) {
                g.setColor(BG_COLOR);
                g.drawPolygon(coinCenter);
                g.drawPolygon(coinsTopLeft);
                g.drawPolygon(coinsTopCenter);
                g.drawPolygon(coinsTopRight);
                g.drawPolygon(coinsLeftCenter);
                g.drawPolygon(coinsRightCenter);
                g.drawPolygon(coinsBottomLeft);
                g.drawPolygon(coinsBottomCenter);
                g.drawPolygon(coinsBottomRight);
                g.setColor(new Color(51, 51, 51, 40));
                g.fillPolygon(coinCenter);
                g.fillPolygon(coinsTopLeft);
                g.fillPolygon(coinsTopCenter);
                g.fillPolygon(coinsTopRight);
                g.fillPolygon(coinsLeftCenter);
                g.fillPolygon(coinsRightCenter);
                g.fillPolygon(coinsBottomLeft);
                g.fillPolygon(coinsBottomCenter);
                g.fillPolygon(coinsBottomRight);
                g.setColor(new Color(0, 153, 204, 120));
                g.setColor(Color.cyan);
                g.drawString(String.valueOf(coinsOnGround().getName() + ": " + NumberFormat.getIntegerInstance().format(dropValue)), px + 2, py);

            }
        }
    }

    //Formats The Time
    private String formatTime(final long time) {
        final int sec = (int) (time / 1000), d = sec / 86400, h = sec / 3600, m = sec / 60 % 60, s = sec % 60;
        return (d < 10 ? "0" + d : d) + ":" + (h < 10 ? "0" + h : h) + ":" + (m < 10 ? "0" + m : m) + ":" + (s < 10 ? "0" + s : s);
    }

    private boolean hasNoCoins() {
        RS2Interface noCoinsInterface = client.getInterface(244);
        if (noCoinsInterface != null && client.getInterface(244).getChild(2).getMessage().equalsIgnoreCase("Ah, outerlander...")) {
            return true;
        }
        return false;
    }

    private boolean hasChatInterface() {
        RS2Interface chatInterface = client.getInterface(64);
        if (chatInterface != null) {
            return true;
        }
        return false;
    }

    private boolean hasPeerChatInterface() {
        RS2Interface chatInterface = client.getInterface(65);
        if (chatInterface != null) {
            return true;
        }
        return false;
    }

    private boolean hasPeerNPCChatInterface() {
        RS2Interface chatInterface = client.getInterface(243);
        if (chatInterface != null) {
            return true;
        }
        return false;
    }

    private boolean hasJarvaldInterface() {
        RS2Interface chatInterface = client.getInterface(242);
        if (chatInterface != null) {
            return true;
        }
        return false;
    }

    private boolean hasNPCChatInterface() {
        RS2Interface npcInterface = client.getInterface(241);
        if (npcInterface != null) {
            return true;
        }
        return false;
    }

    private boolean hasSailInterface() {
        RS2Interface optionInterface = client.getInterface(224);
        if (optionInterface != null) {
            return true;
        }
        return false;
    }

    private boolean hasOptionInterface() {
        RS2Interface optionInterface = client.getInterface(228);
        if (optionInterface != null) {
            return true;
        }
        return false;
    }

    private boolean canToggleRun() {
        return client.getRunEnergy() >= random(25, 100) && !isRunning();
    }

    private boolean inArea(final Area area, int plane) {
        final Position player = myPlayer().getPosition();
        final int getPlane = client.getPlane();

        return area.contains(player) && getPlane == plane;
    }

    public void checkVersion() {

        try {
            URL url = new URL("http://purplekscripting.com/vsnaper/version.html");
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            String line = reader.readLine();
            startVersionTime = System.currentTimeMillis();
            double versioncheck = Double.parseDouble(line);
            if (PROPS.version() == versioncheck && checkedVersion == false){
                log("You are running the most recent version of vSnaper");
                checkedVersion = true;
            } else if(PROPS.version() != versioncheck && !versionGUI.isShowing()) {
                versionGUI.setTitle("vSnaper " + PROPS.version());
                versionGUI.instructionText.setText("You are currently running an out dated version of vSnaper. Although you can still run the script," +
                        " it is highly advised you update it for a better more realistic outcome.  \n\nYou can download the script on the offical thread button below.");
                snapeGUI.setVisible(false);
                versionGUI.setVisible(true);
                warn("You are running an outdated version of vSnaper");
            }
        }
        catch (IOException e) {
            log("Connection failed to server.");
            e.printStackTrace();
        }

    }

    //Condition Interface
    public interface Condition {
        public boolean active();
    }

    private boolean waitFor(Condition c, long timeout) {
        Timer time = new Timer(timeout);
        while ((time.isRunning()) && (!c.active())) {
            random(20);
        }
        c.active();
        return c.active();
    }

    private void error(final String reason) throws InterruptedException {

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                errorGUI.reasonLabel.setText(errorGUI.reasonLabel.getText() + reason);
                errorGUI.collectedLabel.setText(errorGUI.collectedLabel.getText() + NumberFormat.getIntegerInstance().format(snapeCollected));
                errorGUI.runtimeLabel.setText(errorGUI.runtimeLabel.getText() + formatTime(timeRunning));
                errorGUI.setTitle("Results!");
                errorGUI.setResizable(false);
                errorGUI.setVisible(true);
            }
        });
        stopped = true;
        errorHappened = true;
        stop();
    }


    //Draws the custom mouse
    private void drawMouse(Graphics g1) {
        ((Graphics2D) g1).setRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON));
        Point p = this.client.getMousePosition();
        Graphics2D spinG = (Graphics2D) g1.create();
        Graphics2D spinGRev = (Graphics2D) g1.create();
        spinG.setColor(MOUSE_BORDER_COLOR);
        spinGRev.setColor(MOUSE_COLOR);
        spinG.rotate(System.currentTimeMillis() % 2000d / 2000d * (360d) * 2
                * Math.PI / 180.0, p.x, p.y);
        spinGRev.rotate(System.currentTimeMillis() % 2000d / 2000d * (-360d)
                * 2 * Math.PI / 180.0, p.x, p.y);
        final int outerSize = 20;
        spinGRev.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_ROUND,
                BasicStroke.JOIN_ROUND));
        spinG.drawArc(p.x - (outerSize / 2), p.y - (outerSize / 2), outerSize,
                outerSize, 100, 75);
        spinG.drawArc(p.x - (outerSize / 2), p.y - (outerSize / 2), outerSize,
                outerSize, -100, 75);
        g1.fillOval(p.x - 2, p.y - 2, 5, 5);
    }

    //MouseTrail Class
    private final class MouseTrail {
        private final int SIZE = 30;
        private final double ALPHA_STEP = (255.0 / SIZE);
        private final Point[] points;
        private int index;

        public MouseTrail() {
            points = new Point[SIZE];
            index = 0;
        }

        public void add(final Point p) {
            points[index++] = p;
            index %= SIZE;
        }

        public void draw(final Graphics g) {
            double alpha = 0;

            for (int i = index; i != (index == 0 ? SIZE - 1 : index - 1); i = (i + 1) % SIZE) {
                if (points[i] != null && points[(i + 1) % SIZE] != null) {
                    g.setColor(new Color(0, 153, 204, (int) alpha));
                    g.drawLine(points[i].x, points[i].y, points[(i + 1) % SIZE].x, points[(i + 1) % SIZE].y);
                    alpha += ALPHA_STEP;
                }
            }
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {

        Point mouse = e.getPoint();

        if(hidePaint == true) {
            paintBox = new Rectangle(upX, upY, 180, 25);
        }

        if(hidePaint == false) {
            paintBox = new Rectangle(upX, upY, 180, 100);
        }


        if(paintBox.contains(mouse)) {
            upX = (int) (mouse.getX() - dragUpX);
            upY = (int) (mouse.getY() - dragUpY);

        }

    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {

        Point mouse = e.getPoint();

        if(paintBox.contains(mouse) && hidePaint == true && basicPaint == true){
            hidePaint = false;
        } else if(paintBox.contains(mouse) &&  hidePaint == false && basicPaint == true){
            hidePaint = true;
        }

        if(minimizeBox.contains(mouse) && hidePaint == true && basicPaint == false){
            hidePaint = false;
        } else if(minimizeBox.contains(mouse) &&  hidePaint == false && basicPaint == false){
            hidePaint = true;
        }

    }

    @Override
    public void mouseMoved(MouseEvent e) {

        Point mouse = e.getPoint();

        if(hidePaint == true) {
            paintBox = new Rectangle(upX, upY, 180, 25);
        }

        if(hidePaint == false) {
            paintBox = new Rectangle(upX, upY, 180, 100);
        }

        if(paintBox.contains(mouse)) {
            int x = mouse.x;
            int y = mouse.y;

            dragUpX = x - upX;
            dragUpY = y - upY;
        }
    }


    @Override
    public void onMessage(String message) throws InterruptedException {

        if(message.equals("Oh dear, you are dead!")) {
            error("Player died somehow.");
        }

        if(message.equals("You don't have enough space in your bank account.")) {
            error("Not enough bank space");
        }
    }

    public class Timer {
        private long end;
        private final long start;
        private final long period;

        public Timer(final long period) {
            this.period = period;
            start = System.currentTimeMillis();
            end = start + period;
        }

        public boolean isRunning() {
            return System.currentTimeMillis() < end;
        }

        public void reset() {
            end = System.currentTimeMillis() + period;
        }
    }

    public class ErrorGUI extends JFrame {
        public ErrorGUI() {
            initComponents();
        }

        private void closeButtonMouseClicked(MouseEvent e) {
            this.dispose();
        }

        private Point initialClicke;

        private void initComponents() {
            // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
            bgPanel = new JPanel();
            titleScrollPane = new JScrollPane();
            titleText = new JTextField();
            closeButton = new JLabel();
            reasonLabel = new JLabel();
            collectedLabel = new JLabel();
            runtimeLabel = new JLabel();

            //======== this ========
            setUndecorated(true);
            setBackground(new Color(51, 51, 51, 190));
            Container contentPane = getContentPane();
            addMouseListener(new MouseAdapter() {
                public void mousePressed(MouseEvent e) {
                    initialClicke = e.getPoint();
                    getComponentAt(initialClicke);
                }
            });

            addMouseMotionListener(new MouseMotionAdapter() {
                @Override
                public void mouseDragged(MouseEvent e) {

                    // get location of Window
                    int thisX = getLocation().x;
                    int thisY = getLocation().y;

                    // Determine how much the mouse moved since the initial click
                    int xMoved = (thisX + e.getX()) - (thisX + initialClicke.x);
                    int yMoved = (thisY + e.getY()) - (thisY + initialClicke.y);

                    // Move window to this position
                    int X = thisX + xMoved;
                    int Y = thisY + yMoved;
                    setLocation(X, Y);
                }
            });

            //======== bgPanel ========
            {
                bgPanel.setBackground(new Color(51, 51, 51));
                bgPanel.setBorder(new LineBorder(Color.cyan, 1, true));
                bgPanel.setOpaque(false);

                //======== titleScrollPane ========
                {
                    titleScrollPane.setBackground(new Color(51, 51, 51, 221));
                    titleScrollPane.setBorder(new LineBorder(Color.cyan));
                    titleScrollPane.setOpaque(false);

                    //---- titleText ----
                    titleText.setText("Script Shut Down!");
                    titleText.setForeground(Color.cyan);
                    titleText.setFont(new Font("Verdana", Font.BOLD, 14));
                    titleText.setBorder(null);
                    titleText.setBackground(new Color(0, 0, 0, 0));
                    titleText.setEditable(false);
                    titleText.setFocusable(false);
                    titleText.setHorizontalAlignment(SwingConstants.CENTER);
                    titleScrollPane.setViewportView(titleText);
                }

                //---- closeButton ----
                closeButton.setText("Close");
                closeButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
                closeButton.setBackground(new Color(0, 0, 0, 0));
                closeButton.setForeground(Color.cyan);
                closeButton.setBorder(new LineBorder(Color.cyan));
                closeButton.setHorizontalAlignment(SwingConstants.CENTER);
                closeButton.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        closeButtonMouseClicked(e);
                    }
                });

                //---- reasonLabel ----
                reasonLabel.setText("Reason: ");
                reasonLabel.setForeground(Color.cyan);
                reasonLabel.setFont(new Font("Verdana", Font.BOLD, 12));

                //---- collectedLabel ----
                collectedLabel.setText("Snape Grass Collected: ");
                collectedLabel.setForeground(Color.cyan);
                collectedLabel.setFont(new Font("Verdana", Font.BOLD, 12));

                //---- runtimeLabel ----
                runtimeLabel.setText("Total Runtime: ");
                runtimeLabel.setForeground(Color.cyan);
                runtimeLabel.setFont(new Font("Verdana", Font.BOLD, 12));

                GroupLayout bgPanelLayout = new GroupLayout(bgPanel);
                bgPanel.setLayout(bgPanelLayout);
                bgPanelLayout.setHorizontalGroup(
                        bgPanelLayout.createParallelGroup()
                                .addGroup(bgPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addGroup(bgPanelLayout.createParallelGroup()
                                                .addGroup(bgPanelLayout.createSequentialGroup()
                                                        .addGroup(bgPanelLayout.createParallelGroup()
                                                                .addComponent(collectedLabel)
                                                                .addComponent(runtimeLabel))
                                                        .addGap(0, 0, Short.MAX_VALUE))
                                                .addGroup(bgPanelLayout.createSequentialGroup()
                                                        .addGroup(bgPanelLayout.createParallelGroup()
                                                                .addGroup(bgPanelLayout.createSequentialGroup()
                                                                        .addComponent(reasonLabel)
                                                                        .addGap(0, 278, Short.MAX_VALUE))
                                                                .addComponent(titleScrollPane, GroupLayout.DEFAULT_SIZE, 335, Short.MAX_VALUE)
                                                                .addGroup(GroupLayout.Alignment.TRAILING, bgPanelLayout.createSequentialGroup()
                                                                        .addGap(0, 235, Short.MAX_VALUE)
                                                                        .addComponent(closeButton, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)))
                                                        .addContainerGap())))
                );
                bgPanelLayout.setVerticalGroup(
                        bgPanelLayout.createParallelGroup()
                                .addGroup(bgPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(titleScrollPane, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(reasonLabel)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(collectedLabel)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(runtimeLabel)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                                        .addComponent(closeButton, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
                                        .addContainerGap())
                );
            }

            GroupLayout contentPaneLayout = new GroupLayout(contentPane);
            contentPane.setLayout(contentPaneLayout);
            contentPaneLayout.setHorizontalGroup(
                    contentPaneLayout.createParallelGroup()
                            .addComponent(bgPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            );
            contentPaneLayout.setVerticalGroup(
                    contentPaneLayout.createParallelGroup()
                            .addComponent(bgPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            );
            pack();
            setLocationRelativeTo(getOwner());
            // JFormDesigner - End of component initialization  //GEN-END:initComponents
        }

        // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
        private JPanel bgPanel;
        private JScrollPane titleScrollPane;
        private JTextField titleText;
        private JLabel closeButton;
        private JLabel reasonLabel;
        private JLabel collectedLabel;
        private JLabel runtimeLabel;
        // JFormDesigner - End of variables declaration  //GEN-END:variables
    }


    public class SnapeGUI extends JFrame {
        public SnapeGUI() {
            initComponents();
        }

        private void beginButtonMouseClicked(MouseEvent e) {
            if(usePolygonsButton.isSelected()) {
                usePolygons = true;
            }

            if(useRunButton.isSelected()) {
                useRun = true;
            }
            dispose();
            showGUI = false;
        }

        private void initComponents() {
            // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
            bgPanel = new JPanel();
            titleScrollPane = new JScrollPane();
            titleText = new JTextField();
            beginButton = new JLabel();
            instructionScrollPane = new JScrollPane();
            instructionText = new JTextArea();
            information_Label = new JLabel();
            usePolygonsButton = new JCheckBox();
            useRunButton = new JCheckBox();

            //======== this ========
            setUndecorated(true);
            setBackground(new Color(51, 51, 51, 190));
            setResizable(false);
            setTitle("vSnaper - OSBot Release");
            Container contentPane = getContentPane();

            //======== bgPanel ========
            {
                bgPanel.setBackground(new Color(51, 51, 51));
                bgPanel.setBorder(new LineBorder(Color.cyan, 1, true));
                bgPanel.setOpaque(false);

                //======== titleScrollPane ========
                {
                    titleScrollPane.setBackground(new Color(0, 0, 0, 0));
                    titleScrollPane.setBorder(new LineBorder(Color.cyan));
                    titleScrollPane.setOpaque(false);

                    //---- titleText ----
                    titleText.setText("vSnaper");
                    titleText.setForeground(Color.cyan);
                    titleText.setFont(new Font("Verdana", Font.BOLD, 14));
                    titleText.setBorder(null);
                    titleText.setBackground(new Color(0, 0, 0, 0));
                    titleText.setEditable(false);
                    titleText.setFocusable(false);
                    titleText.setHorizontalAlignment(SwingConstants.CENTER);
                    titleScrollPane.setViewportView(titleText);
                }

                //---- beginButton ----
                beginButton.setText("Begin");
                beginButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
                beginButton.setBackground(new Color(0, 0, 0, 0));
                beginButton.setForeground(Color.cyan);
                beginButton.setBorder(new LineBorder(Color.cyan));
                beginButton.setHorizontalAlignment(SwingConstants.CENTER);
                beginButton.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        beginButtonMouseClicked(e);
                    }
                });

                //======== instructionScrollPane ========
                {
                    instructionScrollPane.setAutoscrolls(true);
                    instructionScrollPane.setBackground(new Color(0, 0, 0, 0));
                    instructionScrollPane.setBorder(new LineBorder(Color.cyan));
                    instructionScrollPane.setOpaque(false);

                    //---- instructionText ----
                }

                //---- information_Label ----
                information_Label.setText("Information");
                information_Label.setFont(new Font("Tahoma", Font.PLAIN, 14));
                information_Label.setBackground(new Color(51, 51, 51));
                information_Label.setForeground(Color.cyan);

                //---- usePolygonsButton ----
                usePolygonsButton.setText("Draw Polygons");
                usePolygonsButton.setFocusPainted(false);
                usePolygonsButton.setBackground(new Color(51, 51, 51));
                usePolygonsButton.setForeground(Color.cyan);
                usePolygonsButton.setOpaque(false);
                usePolygonsButton.setSelected(true);

                //---- useRunButton ----
                useRunButton.setText("Run Support");
                useRunButton.setFocusPainted(false);
                useRunButton.setBackground(new Color(51, 51, 51));
                useRunButton.setForeground(Color.cyan);
                useRunButton.setOpaque(false);
                useRunButton.setSelected(true);

                GroupLayout bgPanelLayout = new GroupLayout(bgPanel);
                bgPanel.setLayout(bgPanelLayout);
                bgPanelLayout.setHorizontalGroup(
                        bgPanelLayout.createParallelGroup()
                                .addGroup(bgPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addGroup(bgPanelLayout.createParallelGroup()
                                                .addGroup(bgPanelLayout.createSequentialGroup()
                                                        .addComponent(information_Label, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE)
                                                        .addGap(0, 0, Short.MAX_VALUE))
                                                .addGroup(bgPanelLayout.createSequentialGroup()
                                                        .addGroup(bgPanelLayout.createParallelGroup()
                                                                .addGroup(bgPanelLayout.createSequentialGroup()
                                                                        .addComponent(instructionScrollPane, GroupLayout.PREFERRED_SIZE, 197, GroupLayout.PREFERRED_SIZE)
                                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addGroup(bgPanelLayout.createParallelGroup()
                                                                                .addComponent(usePolygonsButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                                .addComponent(beginButton, GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)
                                                                                .addGroup(bgPanelLayout.createSequentialGroup()
                                                                                        .addComponent(useRunButton, GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE)
                                                                                        .addGap(0, 0, Short.MAX_VALUE))))
                                                                .addComponent(titleScrollPane, GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE))
                                                        .addContainerGap())))
                );
                bgPanelLayout.setVerticalGroup(
                        bgPanelLayout.createParallelGroup()
                                .addGroup(bgPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(titleScrollPane, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(information_Label)
                                        .addGroup(bgPanelLayout.createParallelGroup()
                                                .addGroup(bgPanelLayout.createSequentialGroup()
                                                        .addGap(7, 7, 7)
                                                        .addComponent(instructionScrollPane, GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE))
                                                .addGroup(bgPanelLayout.createSequentialGroup()
                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(useRunButton, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(usePolygonsButton, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(beginButton, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)))
                                        .addContainerGap())
                );
            }

            GroupLayout contentPaneLayout = new GroupLayout(contentPane);
            contentPane.setLayout(contentPaneLayout);
            contentPaneLayout.setHorizontalGroup(
                    contentPaneLayout.createParallelGroup()
                            .addComponent(bgPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            );
            contentPaneLayout.setVerticalGroup(
                    contentPaneLayout.createParallelGroup()
                            .addComponent(bgPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            );
            pack();
            setLocationRelativeTo(getOwner());
            // JFormDesigner - End of component initialization  //GEN-END:initComponents
        }

        // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
        private JPanel bgPanel;
        private JScrollPane titleScrollPane;
        private JTextField titleText;
        private JLabel beginButton;
        private JScrollPane instructionScrollPane;
        private JTextArea instructionText;
        private JLabel information_Label;
        private JCheckBox usePolygonsButton;
        private JCheckBox useRunButton;
        // JFormDesigner - End of variables declaration  //GEN-END:variables
    }

    public class VersionGUI extends JFrame {
        public VersionGUI() {
            initComponents();
        }

        private void beginButtonMouseClicked(MouseEvent e) {
            if(!showGUI) {
                snapeGUI.setVisible(true);
            }
            dispose();
        }

        private void threadButtonMouseClicked(MouseEvent e) {
            try {
                Desktop.getDesktop().browse(java.net.URI.create("http://osbot.org/forum/topic/8178-vsnaper-fremennik-snape-grass-collector/"));
            } catch (IOException e1) {
                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        private void initComponents() {
            // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
            // Generated using JFormDesigner Evaluation license - Henry Frederick
            bgPanel = new JPanel();
            titleScrollPane = new JScrollPane();
            titleText = new JTextField();
            beginButton = new JLabel();
            threadButton = new JLabel();
            instructionScrollPane = new JScrollPane();
            instructionText = new JTextArea();

            //======== this ========
            setUndecorated(true);
            setBackground(new Color(51, 51, 51, 190));
            setResizable(false);
            setTitle("vFletcher");
            Container contentPane = getContentPane();

            //======== bgPanel ========
            {
                bgPanel.setBackground(new Color(51, 51, 51));
                bgPanel.setBorder(new LineBorder(Color.cyan, 1, true));
                bgPanel.setOpaque(false);


                //======== titleScrollPane ========
                {
                    titleScrollPane.setBackground(new Color(0, 0, 0, 0));
                    titleScrollPane.setBorder(new LineBorder(Color.cyan));
                    titleScrollPane.setOpaque(false);

                    //---- titleText ----
                    titleText.setText("      OUT DATED VERSION DETECTED      ");
                    titleText.setForeground(Color.cyan);
                    titleText.setFont(new Font("Verdana", Font.BOLD, 14));
                    titleText.setBorder(null);
                    titleText.setBackground(new Color(0, 0, 0, 0));
                    titleText.setEditable(false);
                    titleText.setFocusable(false);
                    titleScrollPane.setViewportView(titleText);
                }

                //---- beginButton ----
                beginButton.setText("            Close");
                beginButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
                beginButton.setBackground(new Color(0, 0, 0, 0));
                beginButton.setForeground(Color.cyan);
                beginButton.setBorder(new LineBorder(Color.cyan));
                beginButton.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        beginButtonMouseClicked(e);
                    }
                });

                //---- threadButton ----
                threadButton.setText("           Thread");
                threadButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
                threadButton.setBackground(new Color(0, 0, 0, 0));
                threadButton.setForeground(Color.cyan);
                threadButton.setBorder(new LineBorder(Color.cyan));
                threadButton.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        threadButtonMouseClicked(e);
                    }
                });

                //======== instructionScrollPane ========
                {
                    instructionScrollPane.setAutoscrolls(true);
                    instructionScrollPane.setBackground(new Color(0, 0, 0, 0));
                    instructionScrollPane.setBorder(new LineBorder(Color.cyan));
                    instructionScrollPane.setOpaque(false);

                    //---- instructionText ----
                    instructionText.setEditable(false);
                    instructionText.setLineWrap(true);
                    instructionText.setFocusable(false);
                    instructionText.setBackground(new Color(0, 0, 0, 0));
                    instructionText.setForeground(Color.cyan);
                    instructionText.setFont(new Font("Verdana", Font.PLAIN, 10));
                    instructionText.setWrapStyleWord(true);
                    instructionScrollPane.setViewportView(instructionText);
                }

                GroupLayout bgPanelLayout = new GroupLayout(bgPanel);
                bgPanel.setLayout(bgPanelLayout);
                bgPanelLayout.setHorizontalGroup(
                        bgPanelLayout.createParallelGroup()
                                .addGroup(bgPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addGroup(bgPanelLayout.createParallelGroup()
                                                .addComponent(instructionScrollPane)
                                                .addComponent(titleScrollPane)
                                                .addGroup(GroupLayout.Alignment.TRAILING, bgPanelLayout.createSequentialGroup()
                                                        .addGap(0, 0, Short.MAX_VALUE)
                                                        .addComponent(threadButton, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(beginButton, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)))
                                        .addContainerGap())
                );
                bgPanelLayout.setVerticalGroup(
                        bgPanelLayout.createParallelGroup()
                                .addGroup(bgPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(titleScrollPane, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(instructionScrollPane, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(bgPanelLayout.createParallelGroup()
                                                .addComponent(beginButton, GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE)
                                                .addComponent(threadButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addContainerGap())
                );
            }

            GroupLayout contentPaneLayout = new GroupLayout(contentPane);
            contentPane.setLayout(contentPaneLayout);
            contentPaneLayout.setHorizontalGroup(
                    contentPaneLayout.createParallelGroup()
                            .addComponent(bgPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            );
            contentPaneLayout.setVerticalGroup(
                    contentPaneLayout.createParallelGroup()
                            .addComponent(bgPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            );
            pack();
            setLocationRelativeTo(getOwner());
            // JFormDesigner - End of component initialization  //GEN-END:initComponents
        }

        // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
        // Generated using JFormDesigner Evaluation license - Henry Frederick
        private JPanel bgPanel;
        private JScrollPane titleScrollPane;
        private JTextField titleText;
        private JLabel beginButton;
        private JLabel threadButton;
        private JScrollPane instructionScrollPane;
        private JTextArea instructionText;
        // JFormDesigner - End of variables declaration  //GEN-END:variables
    }


    private enum MultiTaskState{
        CAMERA_PITCH, NULL, ROTATE_CAMERA,
    }

    public class MultiTask implements Runnable {

        private MultiTaskState multiTaskState() throws InterruptedException {
           if(canChangePitch()) {
                return MultiTaskState.CAMERA_PITCH;
            } else if (!canChangePitch() && !client.isMinimapLocked()) {
                return MultiTaskState.ROTATE_CAMERA;
            }
            return MultiTaskState.NULL;
        }

        public void run() {

            while(!showGUI && t.isDaemon() && !stopped) {
                try {
                    switch(multiTaskState()) {
                        case CAMERA_PITCH:
                            if(canChangePitch()) {
                                client.rotateCameraPitch(383);
                                client.getEntitiesOnCursor();
                            }
                            break;
                        case NULL:
                            Thread.sleep(random(550, 750));
                            break;
                        case ROTATE_CAMERA:
                            if(inArea(SGsnape, 0)) {
                                int b = random(1,25);

                                switch(b) {
                                    case 12:
                                        if(random(1,9) == 5) {
                                        client.rotateCameraToAngle(random(279, 308));
                                        }
                                        break;
                                    case 10:
                                        if(random(1,12) == 3) {
                                        client.rotateCameraToAngle(random(296, 329));
                                        }
                                        break;
                                    case 2:
                                        if(random(1,25) == 7) {
                                        client.rotateCameraToAngle(random(78, 159));
                                        }
                                        break;
                                    case 7:
                                        if(random(1,19) == 18) {
                                            client.rotateCameraToAngle(random(296, 329));
                                        }
                                        break;
                                    case 14:
                                        if(random(1,3) == 1) {
                                            client.rotateCameraToAngle(random(296, 329));
                                        }
                                        break;
                                    case 25:
                                        if(random(1,6) == 3) {
                                        client.rotateCameraToAngle(random(240, 359));
                                        }
                                        break;
                                    default:
                                        Thread.sleep(500);
                                }
                            } else if(!inArea(SGsnape,0)) {
                                Thread.sleep(500);
                            }
                            break;
                    }
                }
                catch (InterruptedException e) {
                }
                try {
                    Thread.sleep(random(50, 250));
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }

        private boolean canChangePitch() {
            return client.getCameraPitch() < 375;
        }
    }
}
